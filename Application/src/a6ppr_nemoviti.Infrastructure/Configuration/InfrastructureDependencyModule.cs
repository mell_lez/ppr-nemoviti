﻿using Autofac;

namespace a6ppr_nemoviti.Infrastructure.Configuration
{
    public class InfrastructureDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
